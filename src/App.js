import "./App.css"
import { useEffect, useState } from "react"

import imageIcon from "./static/image-icon.png"
import otherIcon from "./static/other-icon.png"
import videoIcon from "./static/video-icon.png"
import newFileIcon from "./static/new-file-icon.png"
import newFolderIcon from "./static/new-folder-icon.png"

function App() {
  const [data, setdata] = useState({
    Documents: ["Document1.jpg", "Document2.jpg", "Document3.jpg"],
    Desktop: ["Screenshot1.jpg", "videopal.mp4"],
    Downloads: {
      Drivers: ["Printerdriver.dmg", "cameradriver.dmg"],
      Applications: [
        "Webstorm.dmg",
        "Pycharm.dmg",
        "FileZila.dmg",
        "Mattermost.dmg",
      ],
    },
  })

  const [selectedFolder, setSelectedFolder] = useState("")

  const KeyValuePair = ({ keyName, value }) => {
    const [show, setShow] = useState(false)
    return (
      <div>
        {!show && <img src="https://cutewallpaper.org/24/arrow-png-white/white-3d393-arrow-02e13-png-45c85-white-343b8-arrow-20af5-transparent-01a5d-background-89c20-freeiconspng.png" height={"10px"} />}
        {show && (
          <img
            src="https://www.freeiconspng.com/thumbs/white-arrow-png/white-down-arrow-png-2.png"
            height={"10px"}
          />
        )}
        <span id={keyName}>
          <button
            onClick={() => {
              setShow((show) => !show)
              document.getElementById(keyName).style.border = "#87B6FA 1px solid"
            }}
            onBlur={() => {
              document.getElementById(keyName).style.border = "#282c34"
            }}
          >
            {keyName}
          </button>{" "}<button>
            <img src={newFileIcon} height={"10px"} />{" "}
          </button>
          <button>
            <img src={newFolderIcon} height={"10px"} />
          </button>
        </span>
        {show && (
          <div className="inner" key={keyName} >
            <DisplayFolders folders={value} />
          </div>
        )}
      </div>
    )
  }

  function DisplayFolders({ folders }) {
    if (
      typeof folders === "object" &&
      !Array.isArray(folders) &&
      folders !== null
    ) {
      return (
        <div>
          {Object.keys(folders).map((val, i) => {
            return <KeyValuePair keyName={val} value={folders[val]} />
          })}
        </div>
      )
    } else {
      return (
        <div>
          {folders.map((val, i) => {
            const type = val.split(".")[1]
            return <p key={i}><img src={type === "jpg" || type === "png" ? imageIcon : type === "mp4" ? videoIcon : otherIcon} height={"10px"} />{" "}{val}</p>
          })}
        </div>
      )
    }
  }

  return (
    <div className="App-header">
      <DisplayFolders folders={data} />
    </div >
  )
}

export default App
